# Máquinas para a 3a Unidade de Sistemas Multimídia

## Intruções

* Requisitos: VirbualBox e Vagrant

```
git clone git@gitlab.com:lssn.oliveira/sm171machines.git
cd sm171machines.git
vagrant up
```

São criadas 4 máquinas

* **gw**: Gateway
* **host[1-3]**: Máquinas da rede com acesso a Internet através do **gw**

Para acessar o gateway:

```
vagrant ssh gw
```

Para acessar o host1:

```
vagrant ssh host1
```

De forma semelhante para o **host2** e **host3**

Divirtam-se! ;p
